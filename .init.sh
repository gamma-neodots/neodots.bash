#!/usr/bin/env bash
ldir "HOME" "$HOME"
case "$1" in
install)
	dotdir="$(realpath "$(dirname "${BASH_SOURCE[0]}")")"
	bashrc=$HOME/.bashrc
	[[ -f "$bashrc" ]] || touch "$bashrc"
	grep -qF "for f in $dotdir" "$bashrc" || \
		echo "for f in \"$dotdir/rc.d/\"*\".sh\"; do
	source \"\$f\";
done" >> "$bashrc"
;;
uninstall)
	echo >&2 "Uninstall not supported for bash dots"
;;
esac
