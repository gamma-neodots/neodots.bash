#!/usr/bin/env bash
# {{{ source .profile, globbing, options
#shellcheck disable=1090
source "$HOME/.profile"
shopt -s autocd checkjobs checkwinsize 
shopt -s direxpand dotglob extglob nullglob globstar
shopt -s cmdhist lithist histverify histappend
shopt -s execfail
set -o histexpand
set -o vi # must be set in bash for fzf to see
# }}}
# {{{ Editor
if command -v nvr-visual >/dev/null; then
	# workaround opening directories
	export SUDO_EDITOR=nvim
	export GIT_EDITOR='nvr --remote-wait-silent'
	export VISUAL=nvr-visual
	export EDITOR=nvim
elif command -v nvim >/dev/null; then
	export VISUAL=nvim
	export EDITOR=nvim
elif command -v vim >/dev/null; then
	export VISUAL=vim
	export EDITOR=vim
fi
export XIVIEWER=feh
# }}}
# {{{ History
HISTSIZE=10000
HISTFILESIZE=200000
# ignore repeated, empty commands
HISTCONTROL=ignoreboth
HISTIGNORE='ls *:l *:ll *:la *:pwd:clear:history:g [cs]'
HISTTIMEFORMAT="%F %T"
# }}}
# {{{ PS1
get_unique_path() {
	local dir cur_path cur_dir i
	local -a matching
	local -a paths
	local expanded="${1@P}"
	local trunc_path="${expanded%%/*}"
	IFS=/ read -r -a paths <<< "${expanded#$trunc_path}"
	# prefix of absolute path
	cur_path="${2:-$PWD}"
	cur_path="${cur_path%${expanded#*/}}"
	for dir in "${paths[@]}"; do
		cur_dir=''
		for (( i=0; i<${#dir}; i++ )); do
			cur_dir+="${dir:$i:1}"
			matching=("$cur_path/$cur_dir"*/)
			if [[ ${#matching[@]} -eq 1 ]]; then
				break
			fi
		done
		trunc_path+="${cur_dir}/"
		cur_path+="$dir/"
	done
	echo "${trunc_path:-/}"
}
_ps1_color() {
	if (( $(stat -c "%u" . ) == UID )); then
		echo -n $'\e[34m'
	elif [[ -w . ]]; then
		echo -n $'\e[33m'
	else
		echo -n $'\e[35m'
	fi
}
_ps1_right() {
	local ret="$?"
	(( ret )) && printf '\e[400C\e[6D\e[31m%4s ↵\e[0m\r' "$ret"
}
# shellcheck disable=2016
PS1='\[$(_ps1_right)\]\[$(_ps1_color)\]$(get_unique_path \\\w)\[\e[0m\] '
unset ps1_header p l
# }}}
# {{{ Colorized pager
export LESS_TERMCAP_mb=$'\e[1;31m'
export LESS_TERMCAP_md=$'\e[1;31m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[1;44;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;32m'
# }}}
# vim: set foldmethod=marker:
