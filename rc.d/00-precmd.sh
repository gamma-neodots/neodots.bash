#!/usr/bin/env bash
if [[ -n "${SSH_CONNECTION:-}" ]] && [[ -z "${DTACH:-}${TMUX_VERSION:-}" ]]; then
	if command -v tmux >/dev/null; then
		if tmux has-session 2>/dev/null; then
			ss="$(tmux list-sessions -F "#S")"
			ret="$(tmux new-session -t "${ss%%$'\n'*}")"
			[[ "$ret" = '[detached'* ]] && tmux kill-session -t "${ret:24: -2}"
			unset ss ret
		else tmux new-session; fi
	elif command -v dtach >/dev/null; then (
		export DTACH="$PREFIX/tmp/dtach-$USER/sock"
		dtach -A "$DTACH" "$SHELL"
	) fi
fi
