#!/usr/bin/env bash
# {{{ Edit aliases, functions, commands
# shellcheck disable=2139
alias ealiases="$VISUAL ${BASH_SOURCE[0]}"
alias efn='shopt -s extdebug; unalias efn; efn'
# relies on vi commands
[[ $VISUAL = *vi* ]] && {
	ewh() {
		case $(type -t "$1") in
			function ) efn "$1" ;;
			alias ) ealiases -c "/$1=" ;;
			file )
				local f
				f="$(realpath "$(type -p "$1")")"
				if [[ $(file "$f") = *text* ]]
				then $VISUAL "$f"
				else $VISUAL "$f" -c ':set ro' -c ':%!xxd'; fi
				;;
		esac
	}
	eal() { ealiases -c "/$1="; }
	efn() {
		set "$(declare -F "$1")"
		$VISUAL -c ":$2" "${@:3}"
	}
	complete -c ewh
	complete -A function efn
	complete -A alias eal
}
# }}}
# {{{ ls aliases
alias ls='ls --color=tty'
alias l='ls -lFh'     #size,show type,human readable
alias la='ls -lAFh'   #long list,show almost all,show type,human readable
alias lr='ls -tRFh'   #sorted by date,recursive,show type,human readable
alias ll='ls -l'      #long list
alias ldot='ls -ld .*'
alias lS='ls -1FSsh'
alias lart='ls -1Fcart'
alias lrt='ls -1Fcrt'
alias lf='ls -AF'    # list almost all, include type
alias lt='ls -ltFh'  # sort by modified time
alias lat='ls -ltuh' # sort by access time
alias lct='ls -ltch' # sort by ctime
alias sl='ls -r'     # no steam locomotive here
# }}}
# {{{ sudo
alias _='sudo'
alias _i='sudo -i'
alias _u='sudo su'
alias _e='sudoedit'
alias sudo='sudo ' # ensures sudo <aliased command> keeps the alias
# }}}
# {{{ one-letter
alias g=git
complete -o bashdefault -o default -o nospace -F _git g
alias p='ps -f'
alias v="$VISUAL"
alias H=head
alias T=tail
alias G=grep
alias L=less
#shellcheck disable=2164
d() {
	# cover no arg case
	if ! (( $# )) || [[ -d "$1" ]]; then
		# d: cd to path
		cd "$@"
	elif [[ -e "$1" ]]; then
		# *: cd to parent
		cd "${1%/*}"
	else
		# _: make path and cd there
		mkdir -vp "$1" && cd "$1"
	fi
}
#shellcheck disable=2164
D() { # d, but absolute
	if ! (( $# )) || [[ -d "$1" ]]; then cd -P "$@"
	elif [[ -e "$1" ]]; then cd "$(realpath "${1%/*}")"
	else mkdir -vp "$1" && cd -P "$1"
	fi
}
# shellcheck disable=2034
NUL=/dev/null
# }}}
# {{{ Interactive cp/rm/mv
alias rm='rm -I'
alias cp='cp -i --reflink=auto' # lightweight copy when possible
alias mv='mv -i'
# }}}
# {{{ Replace grep
if command -v rg >/dev/null 2>&1; then
	alias grep='rg -N'
	alias sgrep='rg -n -C 5'
else
	alias grep='grep --color'
	# use grep as a cheap rg/ag replacemnet
	alias ag='grep -R -n -H --exclude-dir={.git,.svn,CVS} '
	alias rg='grep -R -n -H --exclude-dir={.git,.svn,CVS} '
	alias sgrep='grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS} '
fi
# }}}
# {{{ Find/fd
if command -v fd >/dev/null; then
	alias fd='fd --type directory'
	alias ff='fd --type file'
	alias fl='fd --type symlink'
else
	alias fd='find . -type d -name'
	alias ff='find . -type f -name'
	alias fl='find . -type l -name'
fi
# }}}
# {{{ fzf
# Use tmux version if in tmux session
# shellcheck disable=2034
if [[ -z "${TMUX:-}" ]] && FZF_TMUX=1; then
	alias fzf='fzf-tmux'
fi
# use locate database (`cut` off the working dir)
if command -v locate > /dev/null 2>&1; then
	# shellcheck disable=2016
	export FZF_DEFAULT_COMMAND='p=$(realpath $(pwd)); command locate "$p" | cut -b$(( ${#p} + 2 ))-'
	alias fzfa='locate "*" | fzf'
fi

ffg() {
	rg --files-with-matches "$*" | \
		fzf --multi --preview-window=up:60% \
		--preview "rg --color=ansi --line-number --context 1 \"$*\" {+}" \
		--bind="alt-e:execute($VISUAL {+} -c \":silent grep! \\\"$*\\\"\" -c \":copen\")"
} # group as one string
# }}}
# {{{ grc colouring
if command -v grc >/dev/null 2>&1; then
	alias GRC='grc -es ' # space ensures grc <cmd> expands
	complete -F _sudo grc # lazy fix
	alias LA='GRC ls -lFAhb --color'
	alias LS='GRC ls -lFhb --color'
	alias df='GRC df -hT'
	alias dig='GRC dig'
	alias docker='GRC docker'
	alias docker-machine='GRC docker-machine'
	alias env='GRC env'
	alias gcc='GRC gcc'
	alias ip='GRC ip'
	alias lsblk='GRC lsblk'
	alias lspci='GRC lspci'
	alias mount='GRC mount'
	alias nmap='GRC nmap'
	alias ping='GRC ping'
	alias ps='GRC ps --columns $COLUMNS'
	alias traceroute='GRC traceroute'
	du() {
		tabs -5
		local args=(-sh)
		trap '{tabs -4;}' INT
		# if no args, depth=1 on cwd
		(( $# )) || args=(-h -d 1 .)
		command du "${args[@]}" "$@" 2>&1 |
			sort -h | grcat "${PREFIX:-/usr}/share/grc/conf.du"
		tabs -4
	}
	lsl() { grc ls -lFhb "$@" --color | less -R; }
	#compdef
fi
# }}}
# {{{ other colouring
grepl() { grep "$@" --color=always | less -R; }
# complete ???? grepl
alias hi='highlight -O ansi'
hial() { command -v "$1" | sed "s/.*='\\(.*\\)'$/\\1/" | highlight -O ansi --syntax=bash; }
hifn() { type "$1" | tail -n +2 | highlight -O ansi --syntax=bash; }
hiwh() {
	for arg in "$@"; do
	case $(type -t "$arg") in
		file ) highlight -O ansi "$(type -p "$arg")" ;;
		function ) hifn "$arg" ;;
		alias ) hial "$arg" ;;
	esac 
	done
}
complete -A alias hial
complete -A function hifn
complete -c hiwh
# }}}
# {{{ rsync defaults
alias rscp='rsync -aP'
# }}}
# {{{ multiplexing
if command -v tmux >/dev/null; then
	# {{{ Update it all
	if [[ -n ${TMUX:-} ]]; then
		if command -v aurman >/dev/null 2>&1; then
			alias UU='tmux split aurman -Syu; git-all u'
		elif command -v pkg >/dev/null 2>&1; then
			alias UU='tmux split pkg update; git-all u'
		elif command -v apt >/dev/null 2>&1; then
			alias UU='tmux split sh -c "sudo apt update && sudo apt full-upgrade"; git-all u'
		fi
	else
		if command -v aurman >/dev/null 2>&1; then
			alias UU='tmux new git-all u \; split aurman -Syu'
		elif command -v pkg >/dev/null 2>&1; then
			alias UU='tmux new git-all u \; split pkg update'
		elif command -v apt >/dev/null 2>&1; then
			alias UU='tmux new git-all u \; split sh -c "sudo apt update && sudo apt upgrade"'
		fi
		# }}}
		# {{{ tmax
		tmax() {
			if [[ -z $1 ]]; then
				if tmux has-session; then
					tmux kill-session -t "$(tmux new-session -t "$(tmux list-sessions -F "#S" | head -n 1)" | head -c -3 | tail -c +25)"
				else tmux new-session
				fi
			else
				if tmux has-session -t "$1"; then
					tmux kill-session -t "$(tmux new-session -t "$1" | head -c -3 | tail -c +25)"
				else tmux new-session -s "$1"
				fi
			fi
		}
		# }}}
	fi
fi
if command -v dtach >/dev/null && [[ -z ${DTACH:-} ]]; then
	dtch() {
		local d="$PREFIX/tmp/dtach-$USER"
		mkdir -p "$d"
		( export DTACH="$d/${1:-sock}"; dtach -A "$DTACH" "$SHELL" )
	}
fi
# }}}
# {{{ *TeX
alias tex2txt='pandoc -f latex -t plain'
alias texmk='latexmk'         # default
alias texc='latexmk -c -norc' # leave output files (*.dvi/ps/pdf)
alias texC='latexmk -C -norc' # leave only *.tex files
# }}}
# {{{ wrappers/defaults
# ${file%.*} strips file of its extension
alias nyx='sudo -u tor nyx -s ~tor/control'
alias lkb='xmodmap -pke; xmodmap'           # List current keybindings
alias htip='HTOPRC=$HOME/.config/htop/minrc htop'
alias mpvr='mpv --audio-display=no'
alias ytdl='youtube-dl'
alias nterm='nohup urxvt >/dev/null & exit' # launch new terminal in .
alias pycalc='python3 -ic "from cmath import *"'
alias rust-c='rustc --out-dir build -O'
gc-c() { gcc -O3 "$1" -o "build/${1%.*}" "${@:2}"; }
gh-c() { ghc -Odph -dynamic "$1" -o "build/${1%.*}" -odir .build -hidir .build -tmpdir . "${@:2}"; }
hscalc() { echo -e "main=putStrLn $ show $ $*" | runhaskell; }
join_by() { local IFS="$1"; shift && echo "$*"; }
notefo() { notes find "$1" | notes open; }
redditcodecopy() { sed -e 's/^/    /' -e 's/\t/    /g' "$@" | xsel -b; }
xevkb() { xev -event keyboard "$@" | grep -E -o '(keycode(.)+\)|XLookup.+[1-9].+)'; }
# }}}
# {{{ i3tool
if [[ -e "$HOME/.local/lib/i3/i3tool.sh" ]]; then
	#shellcheck disable=1090
	. "$HOME/.local/lib/i3/i3tool.sh"
	alias i3=i3tool
	alias i3m='i3tool msg'
	i3e(){
		i3tool --no-startup-id exec "${1:c:P}" "${@:2}"
	}
	ttv() {
		i3tool exec xdg-open 'https://twitch.tv/popout/'"$1"'/chat?popout='
		streamlink "twitch.tv/$1" "${@:2}"
	}
fi
# }}}
# {{{ Vi muscle memory
alias :e="$VISUAL"
alias :sp="$VISUAL -o"
alias :vsp="$VISUAL -O"
alias :x="wait && exit"
alias :wq="wait && exit"
alias :q="exit"
# }}}
# vim: set foldmethod=marker:
